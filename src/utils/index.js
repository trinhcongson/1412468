import axios from 'axios';

export const axiosInstance = axios.create({
  baseURL: 'https://food-delivery-server.herokuapp.com'
});
