import React, { Component } from 'react';
import { Image, StyleSheet, Dimensions } from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Text,
  Left,
  Title,
  Subtitle
} from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import chunk from 'lodash/fp/chunk';
import map from 'lodash/fp/map';

import Footer from './Footer';
import Header from './Header';
import { axiosInstance as axios } from '../utils';

export default class Merchant extends Component {
  static navigationOptions = () => ({
    headerLeft: <Header />
  });

  state = {
    listMerchant: undefined
  };

  fetchMerchantListForIndexPage = async () => {
    try {
      const result = await axios.get('/restaurant/getAll/10&1');
      if (result) {
        const newListMerchant = chunk(2)(result.data.data);
        this.setState({ listMerchant: newListMerchant });
      }
    } catch (error) {
      console.error(error);
    }
  };

  showMerchantDetail = id => {
    this.props.navigation.navigate('MerchantDetail', { id });
  };

  componentDidMount() {
    this.fetchMerchantListForIndexPage();
  }

  render() {
    return (
      <Container>
        <Content>
          <Card style={styles.listCard}>
            <Grid>
              <Row style={styles.headerRow}>
                <Col>
                  <Title>FastFood</Title>
                  <Subtitle>Too fast, Too delicious</Subtitle>
                </Col>
              </Row>
              {this.state.listMerchant &&
                map(row => (
                  <Row key={Math.random()}>
                    {map(item => (
                      <Card
                        transparent
                        style={styles.merchantCard}
                        key={Math.random()}
                      >
                        <CardItem
                          cardBody
                          onPress={() => this.showMerchantDetail(item.info.id)}
                        >
                          <Image
                            source={{
                              uri: item.info.image
                            }}
                            style={styles.merchantImage}
                          />
                        </CardItem>
                        <CardItem style={styles.merchantNameCard}>
                          <Left>
                            <Text
                              onPress={() =>
                                this.showMerchantDetail(item.info.id)
                              }
                              style={styles.merchantName}
                            >
                              {item.info.name}
                            </Text>
                          </Left>
                        </CardItem>
                        <CardItem style={styles.merchantRatingCard}>
                          <Left>
                            <Text style={styles.merchantRating} note>
                              Rating: {item.info.rating}
                            </Text>
                          </Left>
                        </CardItem>
                      </Card>
                    ))(row)}
                  </Row>
                ))(this.state.listMerchant)}
            </Grid>
          </Card>
        </Content>
        <Footer />
      </Container>
    );
  }
}

const { width: windowWidth, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  listCard: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderRadius: 10,
    marginLeft: 4,
    marginRight: 4
  },
  merchantCard: {
    width: '48%',
    marginLeft: 4
  },
  merchantName: {
    fontWeight: 'bold',
    fontSize: 10,
    marginLeft: 0
  },
  merchantNameCard: {
    paddingLeft: 0,
    paddingRight: 0
  },
  merchantRating: {
    fontWeight: 'bold',
    fontSize: 10,
    marginLeft: 0
  },
  merchantRatingCard: {
    paddingLeft: 0,
    paddingRight: 0
  },
  merchantImage: {
    height: windowWidth * 0.48,
    width: null,
    flex: 1,
    borderRadius: 10
  },
  headerRow: {
    marginTop: 8,
    marginBottom: 8
  }
});
