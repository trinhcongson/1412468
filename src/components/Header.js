import React from 'react';
import {
  Header,
  Button,
  Left,
  Body,
  Icon,
  Title,
  Container
} from 'native-base';
import { StyleSheet, Dimensions } from 'react-native';

class CustomHeader extends React.Component {
  render() {
    return (
      <Header style={styles.header}>
        <Left style={styles.left}>
          <Button transparent>
            <Icon name="menu" />
          </Button>
        </Left>
        <Body style={styles.body}>
          <Title>Header</Title>
        </Body>
      </Header>
    );
  }
}

const { width: windowWidth, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  header: {
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: 0,
    height: 44,
    backgroundColor: '#fff'
  },
  left: {
    width: windowWidth * 0.2,
    marginLeft: 12
  },
  body: {
    width: windowWidth * 0.55
  }
});

export default CustomHeader;
