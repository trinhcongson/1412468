import React, { Component } from 'react';
import { Image } from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  View
} from 'native-base';

const listProduct = [1, 2, 3, 4, 5];

export default class Basket extends Component {
  render() {
    return (
      <Container>
        <Content>
          {listProduct &&
            listProduct.map(merchant => (
              <Card key={Math.random()}>
                <CardItem>
                  <Left>
                    <Thumbnail
                      source={{
                        uri:
                          'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxIQEBUPEBISEBAVEBIWEBUQFRYSFxURGBUWFhUSFhUYHSggGBolGxgVITEhJSkrLi4uGB8zODUtNygtLisBCgoKDg0OGxAQGi0lHyUrLS0tKy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tN//AABEIAOEA4QMBEQACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABQYBBAcDAv/EAD8QAAIBAQQDDAgFBAMBAAAAAAABAgMEBQYRITFREhMiNEFhcXKBkbHBMlJic5KhstEjQlOCwhQVJDNDovBj/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAEEBQMGAv/EACoRAQACAQIFAwQDAQEAAAAAAAABAgMEERIhMTNBBVFxEyIyYRQjgUIV/9oADAMBAAIRAxEAPwDuIAAAAAAAAAAA8qteMNMmora2kTETPSHxbJWvWUbXxFZoa6qfNDOXgdIwZJ8OFtXijy0amMKC1RqS7IrxeZ0jSXlwn1HFHh4vGcOSlPvR9xo7e7nPqlfYWNIfpS+JD+Hb3I9Ur7PanjGi9cKkeyL88z4nSXfcepY56w3aGI7NP/kUeunH5s5zgyR4WK6zFbykqVojNZwkpLbFpo5zWY6w7VyVt0l7EOgAAAAAAAAAAAAAAAAAADA+JSyHOeiJ5IW8sTUKWhN1JbIal0y1eJ3x6a9uanm12LHyVu3Ypr1NEWqS9jS/ifkXKaSlerNyeoZLdENWqym85ylN7ZNyfeyzFKx0hUtltbrL4PtzABCQASgIH1SqOLzi3F7Ytxfej5mkT1h9xktHSUxYsT2in6UlVWyev4lpK19LS3Rcx6/JXrzWS7cU0amSm3Sl7fo580vvkVMmmvX4aGHX478vKcjPPSnmuQr9F2J35w+kExLISAAAAAAAAAAAABhsCLve/KVnWUnup8kI6+3YdceG2Tp0Vs+qpijn1Uq9L8rWhtSe5h6kdC7X+Y0cenpSP2xM+rvln9IwsRGyqBIEBIEAAAAABIEJCUJC7L4rWd8CWceWEtMezZ2FfLgpf5WMOqyY5/S63Pf9O0aM9xU9SX8Xy+JnZMFqNvBq6Zf1KXTOK2yAAAAAAAAAAAMNg3VTEGJ9znSoNOWqU9aXNHa+cuafTcXOzK1Wu2ngoqE5uTbk229Lb0vM0oiIjaGRaZtO8vkbPkAAAAAAAAAAAAAAAynk808mtTREkTtPJbMP4n1UrQ9ijUfhL7mfn03mrY0ut/5ut0ZZlJrbxPR9AAAAAAAAAMNhE8lLxNiHdt0aL4OlTmvzezF7Ocv6fT/9WY+s1k78FOirl9lcwJAgAAAASBAAAACUgQEAAABEwEp2lYsNYgdFqlVedPVGT/J0+z4FHUaff7oaek1nB9lui8wlnpRndG1ExMbw+gkAAAAADDYFTxbfeWdnpPJ5fiyXIvUXPtLumwb/AHSyddqpj7KKeaLIAgAEgABu+qcHJ5RTk9kVm/kfE3iOr7rS1ukN+jcdpnqozXWyj9RznUY48u1dJmn/AJblPClpeve49aX2TOc6ujvHp+Xy9o4Nr8s6S6HJ/wAUfM62vtL7/wDNv7wzLBtbknSfTul5ERra/s/8y/vDwqYTtK/Tl1ZPzSPr+ZR8T6fkjo069yWiHpUpZbY5S8DrXUUny4W0mav/AC0JxaeTTT2NZHWL1no42pavWHyS+OQAAAAkCJWnCd97lqz1Xo/4pPk9h82woanB/wBQ1tDqpjalv8XNMz4lsMkgAAAYzAiMSXp/T0nl/slogvGXQvsdsGL6llTV5/pU/bncpNvNvNt5tvlfKzXiNuUPPTabc5YPp8hAAjm+qcHJ7mKcm9SSzb7ERa0RHN9UrNp2iE7d+FK1TTPKlHn4Uu5aF2sq5NXWvKq/h9PvbnPJYbHhehT0yi6j9t5ruWgqX1N7eWhj0OOvXml6NnjBZQjGK2RSXgcJm09ZWq4616Q9NyRtD62ZyCWQAGGgMZBGzyr2WE1lOEZr2kn4kxa0dJfFsVLdYQ1swnQnpgnSfsvNdz8jvTU3r15quTQYrdI2V+8MLV6ebglVj7Oh5dV+WZcx6us8pZ2b0+9Occ0HKLTyaafKmsn3FmtonopTWY6wwS+QkCJDxI23TvMOgYXvff6e5m/xYZKXOuSX35zJ1GLgtvHR6DRaj6tNp6wnczgubgSAAPOpLJN6ktfQNt+SJnbq5tfl4O0VpT/KtFNbIrl7dL7TXwY4pT9vNarNOW/6R53cAICSUvcFyO1Nty3MItKWWlvmX3KufUTTlC5pNJ9bnPReLvuylQWVOCW1vTJ9LM2+S153mW5jw0pG0Q3Uj4dWQkAAAAAAAAAAPlojkNK8bqpV1lUgm+SS0SXajpXLavRwy6fHlja8KXf9wuzcNS3VNvJZ6JJ69O3VrNDBqOOdpYur0X0o4o6IUtqX7AAG3ddulZ6sasdSfCW2PKjlmxxejvgzTivvDplCqpxUovNNJprYzGmOGdnpa2i1YmHsH0AAK7jG373R3pelUeX7F6Xku0saXHxX3ln+oZuDHwx1lRDWhhAQAASu2BI/gze2q/pRl6ufvbnpsfYs5VaQAAAAAAAAAAAAAABX8bQzs2eycH4rzLOk7ij6hH9ShGrDz4AAAXXBV4bqnKjLXDTHqP7PxRl6vHtbeG36dn4qbT4WgqtMA+WyUT03c6xPbN9tM8vRg9wuzW+/M09NThpu89rsvHk29kSWlMAEgRPQXzBK/wAbPbUn5LyMrVz/AGt/06NsMLCVl8AwEAADISAAAGAjkAAMhIBC4vjnZJ8zg/8Aujtp5/shT10f0y54bDzoAAASFwWzebRCf5W9zLqyyXyeT7CvqKcVJWdJl4MrphkvSRO4Eta8a+90p1PVg32paD6pG9ocs1uGky5a3m83rel9Jt1jaNnmLTvaZYJfIAAAl0DBy/xYPbKf1yXkZGo7kvQ6DtQnTgutC+ako2erKLcZKlNprWmlrR9Yo3tG7jntMY7THs5//eLR+vU+Jmt9DH7PP/ys3uf3i0fr1PiY+hj9j+Vm9z+8Wj9ap8TH0KbdD+Vm95XrDFaU7LCc5OUnu83J5t8Jozc9YrkmIbujvNsUTZKnFZAI+/qsoWarKLcZKGaa0NM+8cb2iHHUWmuOZhQP7xaP1qnxM1foU9oef/lZfeT+8Wj9ap8TJ+hi9j+Vm95P7xaP1qnxMicOP2P5WXfrK+YfrSnZqcpycpOLzb0t6WZmaIi8xDe01ptjiZSaOSwicUL/ABavVX1I7YO5CrrI/ps5wbDzYAAAAmOu7p1y2nfaFOo9bgt11lofzzMTJXhtMPT6e/HjiW8c3VA4xrbmytcs5xj/AC8ixpq75FPX34cMqCjXeeAAAACXRMJrKyU/3/XIx8/5y9Hoo/qhMnFbR1/cWre5n4HTB+dVfVdq3w5mbTzIAHj/AEdFwlxOn+/65GRqe5L0Wh7EJg4LgwI3EnFKvu2dMX5wr6rtWc0Np5nyABJ5dGwzxWl1X4sxs/cl6TSdmqXRyWkbiNZ2Wr7tnTD+cK2r7NvhzU2tnmmAAAAErzgivnQlD1aj7nk/HMytVXa+7d9Otvj2WIrr+8KpjurwaUNspPuSX8i3oo+6ZZXqk/ZEKeabIkIQAAAJdGwtxWl0P6pGNn7kvSaPswlzktI6/eLVvcz8Dpg7lVfVdq3w5mbTzIAHj/SXRcJcTp/v+uRkanuS9HoexVMHBbGBG4k4pW92zpi/OFfVdq3w5obTzHmAJBJ5dGwxxWl1X4sxs/cl6TSdmEsjktNC/wDi1b3M/BnTF+cK+q7NvhzI23mQgAAAC1YDqcKrHmg+5yXmjP1sdJa/pdudoXHIotXZTcdvh0l7M33uP2NDRdJlkep9awqxeZQAAACSXR8LvOy0uq/FmNn7kvSaPswljitI6/eLVvcz8Dpg7lVfVdq3w5mbTzIAHj/SXRcJcTp/v+uRkanuS9HoexVMHBbGBG4k4pW92zpi/OFfVdq3w5obTzHmAJBJ5dGwxxWl1X4sxs/cl6TR9mEsjktNC/uLVvc1PpZ0xflCvquzb4cyNt5meoQAAABY8DS/Hmv/AJP5Sj9ylrPxhp+mT98ruZuzaU3Ha/EpP2JeKNHRdJhj+p/lWVXLzKAAAACXRcKPOyU/3fVIx8/5y9Ho5/qhMHFbR1+8Wre5n4HTB3Kq+q7VvhzM2nmQABf8LWunGyQjKcYvh5pySfpyMnUVmckzs9Bosla4YiZS39dS/Uh8S+5w4bey19Wnux/XUv1IfEvuTw29j6tPdH4gtdOVlqpTg24PJKSZ94qW445OGpyUnFPNzs2fDznkACTy6NhjitLqvxZjZ+5L0mj7MJZHJaR2IXlZavupLv0HTF+cQr6rtW+HNDaiXmp6gQAAAFjwPxiXun9USlrPxhpemdyV3zM5t7Kljun/AKpddd+5+xd0c85ZPqccoVI0WQAAAAklf8GzzssVslNf9m/Mx9TyyPQ6Cd8MSnjguo6/eLVvcz8Dpg7lVfVdq3w5mbTzIAADbc3kGyd5CNjeQbI3kJRtsBIJPLo2GOK0uq/FmNn7kvSaPswlkclpE4qnlZanOku+SR1wR/ZCrrJ2w2c5NmHm4AAAABaMCU86lWWyEV3tvyKGtnpDV9Mj7rSuWRQ5NhX8b0d1Z1L1akX2NNeaLOlna6h6hXfHuopqsEAAAAPMLrgStnTnDPTGefZJL7My9XX79236ZbfHMe0rOVWmj7+4tW9zPwOmDuVV9V2rfDmZtPMgAAAAAAAAAJPLo2GOK0uq/FmNn7kvSaPswlkclpXsb1crOo8spxXYs5eSLGlje7P9RttiUQ1mD05AAAAAumBKWVKpP1ppdkV95MzNZbe+zb9OrtjmfeVmzKmzV2ad9WbfbPUhyuDy6VpXzR0x24bQrainHjmHMUbW+7zMgQAAASlMN3jvFdSfoS4M+ZPU+x/LM4amkXp+1rR5px359JdGi81mZG2z0XXm0b+4tW9zPwOmHuV+XDVdq3w5mbTzIAAZjeE7SZjig4ZMxxQcMmY3g2kCAAJPLo2GeK0uq/FmNn7kvR6Ts1SrOPha8qBi28N+rbiPoU84rnlnwn4LsNXS4uGu8sDXZvqW4Y8IMtR0UQAAABLpGGrPvdmpx5XHdPplp8zGzW4rzL0mkpwY4hKHFZYkifKJiJjaXMr7sm82ipDk3TcerLSvsa+C3HR5rVY5plmrRLCuEAAACRcsJXzukrPUfDX+tv8ANHZ0rwM3U4OGd6trQ6qLR9Oyav3i1b3M/ArYPzr8rmq7VvhzM23mQAPCJ6ug4Upp2Sm2k3w+T25GTqZn6kvQ6KsThjeEvvMfVXccN5W+CvsbzH1V3DeTgr7I7ENKKstV5LPcPUjpimeKHDU0rGOdoc4Nl5qOsgSCTy6NhnitLqvxZj5+5L0mj7VWpim+d5hvUH+LJcn5I+t07D70+HjtvPRx1uqjHXhjrKiGrDB33AAAABs3dZXWqwpL80kn1dcn3JnLNfgpMu2nx/UyRDqUI5aDGeniNoZAyEqljew5qNdL0eDPob4L79HaXdHk2twyyvUcO9eKFPNFjAAAAA+oSaakm00001oaa1NMiaxMbPqLTWd4Wyjfqr2StTm8qyoz/ctz6S80Z1sE0yRMNamsjLhtE9YhUTSY4AHgl0XCXE6f7/rkZGp7kvR6HsQmDgtsARuI+KVfds6YvzhX1Xas5qbTzHmAJBKYXGhfMbNYqWWUqri9xHtfCfMZk4ZyZZbFNVGLTxPlUq9aU5Oc25Sk823tNGlIrG0Mm95vbeXmfT4AAAAErVgiw5uVdrVwIdOuT8O9mfrMnSsNX03D1ySuZRbAAA17bZo1acqctKkmn9ya24Z3c8lOOs1lzG22WVKpKnL0ovLpXI+1G1jvF67w81lxzS01l4H25AAAAADZPncCAAPH+kui4S4nT/f9cjI1Pcl6PQ9iqYOC2MCNxJxSt7tnTF+cK+q7VvhzQ2nmPMASAZb/APc2wRG3RM82AgAAAAHpZ6Mqk404rOUmkulnza3DXeX3jpOS3DDp13WNUaUaUdUVl0vW335mJe/HaZemxY+CsQ2yHUAAYyArOMbq3yO/wXDguElyw159nhmWtNl4Z2lm6/TcdeKvWFJNVhhAAAAAAAADx/pLouEuJ0/3/XIyNT3Jej0PYqmDgtjAjcScUre7Z0xfnCvqu1b4c0Np5jzAEgAAAAAAlkIW/Bt1ZL+pmtL0U+aPLLt8OkztXm3nhq2vT9PNf7LdVsyKOzUZJAAAA+ZLMImN1AxPc28T3yC/Ck/gl6vRsNLTZ+KOGerD1ulmluKvRBlxnAAAAAAAA8Il0PCUv8SC55/WzI1PcmZej0O30YiEymcFwYEbiJ/4tXqM+8X5Qr6ntTu5qbcTyh5nyAAAAAACQInkmMOXO7TUzkvwovhP1n6q8ytqM8UjaOq9o9NOW0Wno6HCGSSWpajKlvxG3KH0EgAAAAAeNqoRqRcJpSi1k0xEzWd4fN6ReNpc7v255WafrU2+BL+MufxNbBmi8c+rz+p0047cuiMLKmEAAAAAAG5dt51LPLdU3o/NF+i+zbznHJhrfq7Yc9sVt4le7mvynaFkuDUy0wevpW1Gbkw2p8N7Bqq5Y/bYvO9KdCO6qPLZFaZPoR8UxzeeTplzVxxzUO+L6qWl5Pg089EF4y2s08OnrTnPVhajV2yTt4RpYVGAAAAACQI6JC5rqnaZ7mOiC9OWxbFtZwzZopH7W9Np5yz+nRLFZY0oRpwWUUtH3e1mTa02neXoMVIpXhr0bJD7AAAAAAAYYHjarNGpFwmlKLWlMmLTWd4fF6ReNpUO/bgnZ25xznR263Hmlzc5p4dTF42t1YWp0c454q9EKWlEBHMAAAAAImH1CTTUk2mtTWhp7cyLRFuUvqtrVneH1XryqS3c5OUnyyefYK0rXlD6vktbnM83mS5xAEgAAABPIJN0vcdxztLz0xpLXLbzR29JVzaiKRtHVd0+jtlneY5L7YrJGlFQhFRiv/ZvazLtabTvLdx46444a9GwQ6MgAAAAAAAAAHxKKehrNco5+ETG6rX1hRSznZ+DL1H6L6r5OjV0FvDqpjlLL1Pp/HPFTlKpV6MqcnCcXCS1qSyNGt4tG8Mq+O1Z2tDzPpzAAAAAAEgQAAASBA+6VOU2owTlJ6lFZtnza1axvL6pWbTtVarmwpqnaOymv5PyRQy6rflVq6f0/aeLIttOmorJJJJZJLQkilvM9WtEbcofYSAAAAAAAAAAAABhgatuu+nWjuakVLY+VdD5D6pe1ekuWXDTJH3Qqt5YRlHTQlul6s9D7HqfyLuPWR0sys3psxzpKu2qy1KTyqQlB+0su56mW65KWjeJZ+TDkpO1o2eJ0hzAAAkAAAgAPWz2edR7mnGU37Kz7z4tkrWOcumPFe87VjdYLuwlOWmtJU16seFLv1L5lXJrI6Vhfxemzad7zstV3XZSoLKnFLa9bfSyjfJa/WWrjw1xxtEN1Hw7MgAAAAAAAAAAAAAAAAADzqUlJZSSa5U1mTEzHR8zWLRtMIi1YZs9T8m9vbT4Py1fI611GSvlUvosVvCLr4L5adXsnHP5p+R3jWz5VremR4lozwjXWqVKXa0+7I6RrKeXCfTcnh4vC9q9SPxo+41eL3lzn07N7QLC9p9SPxon+Xj95P8Azs3tD2p4RtD1unFdZt9yRznW08bvuPTcnnZuUMGevW7IR82z4nWz4h3r6X7ylbNhezw1xdR+2813ajhbUZLeVrHocVOsbpehRjBbmKUVsisl3HGZmeq1WkVjaHqQ+wAAAAAAAAAAAAAAAAAAAAHyiX0wiJRboRHh8VGTCLdRgGBgkr1fTIfchEEMgZAAAAAAAAAAAH//2Q=='
                      }}
                    />
                    <Body>
                      <Text>NativeBase</Text>
                      <Text note>GeekyAnts</Text>
                    </Body>
                  </Left>
                </CardItem>
                <CardItem cardBody>
                  <Image
                    source={{
                      uri: 'http://hdqwalls.com/wallpapers/small-memory-lp.jpg'
                    }}
                    style={{ height: 200, width: null, flex: 1 }}
                  />
                </CardItem>
                <CardItem>
                  <Left>
                    <Button transparent>
                      <Icon active name="thumbs-up" />
                      <Text>12 Likes</Text>
                    </Button>
                  </Left>
                  <Body>
                    <Button transparent>
                      <Icon active name="chatbubbles" />
                      <Text>4 Comments</Text>
                    </Button>
                  </Body>
                  <Right>
                    <Text>11h ago</Text>
                  </Right>
                </CardItem>
              </Card>
            ))}
        </Content>
        <View>
          <Button danger>
            <Text> Buy now! </Text>
          </Button>
        </View>
      </Container>
    );
  }
}
