import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Container, Form, Item, Label, Input, Button, Text } from 'native-base';

import { axiosInstance as axios } from '../utils';

class Register extends Component {
  static navigationOptions = () => ({
    title: 'Register'
  });

  state = { username: '', password: '', retypePassword: '' };

  handleChangeUserName = text => {
    this.setState({ username: text });
  };

  handleChangePassword = text => {
    this.setState({ password: text });
  };

  handleChangeRetypePassword = text => {
    this.setState({ retypePassword: text });
  };

  handleRegister = async () => {
    const result = await axios.post(`/register`, {
      email: this.state.username,
      password: this.state.password
    });
    if (result.status === 200) {
      this.props.navigation.goBack();
    }
  };

  handleGoToForgetPassword = () => {
    this.props.navigation.navigate('ForgetPassword');
  };

  render() {
    const { username, password, retypePassword } = this.state;
    return (
      <Container>
        <Form>
          <Item stackedLabel style={styles.formItem}>
            <Label style={styles.formItemLabel}>Username</Label>
            <Input onChangeText={this.handleChangeUserName} value={username} />
          </Item>
          <Item stackedLabel style={styles.formItem}>
            <Label style={styles.formItemLabel}>Password</Label>
            <Input
              onChangeText={this.handleChangePassword}
              secureTextEntry
              value={password}
            />
          </Item>
        </Form>
        <Button
          block
          primary
          style={styles.registerButton}
          onPress={this.handleRegister}
        >
          <Text>Register</Text>
        </Button>
        <Button
          block
          transparent
          style={styles.forgetPassword}
          onPress={this.handleGoToForgetPassword}
        >
          <Text>Forget password?</Text>
        </Button>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  registerButton: {
    marginTop: 20,
    marginLeft: 25,
    marginRight: 25
  },
  forgetPassword: {
    marginTop: 20,
    marginLeft: 25,
    marginRight: 25
  },
  formItem: {
    marginLeft: 25,
    marginRight: 25
  },
  formItemLabel: {
    fontSize: 16,
    fontWeight: 'bold'
  }
});

export default Register;
