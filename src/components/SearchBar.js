import React, { Component } from 'react';
import {
  Container,
  Header,
  Item,
  Input,
  Icon,
  Button,
  Text
} from 'native-base';
import { StyleSheet, Dimensions } from 'react-native';

export default class SearchBarExample extends Component {
  render() {
    return (
      <Container style={styles.searchBar}>
        <Header searchBar rounded style={styles.searchBarHeader}>
          <Item style={styles.searchBarInput}>
            <Icon name="ios-search" />
            <Input placeholder="Search" />
          </Item>
          <Button transparent style={styles.searchBarButton}>
            <Text>Search</Text>
          </Button>
        </Header>
      </Container>
    );
  }
}

const { width: windowWidth, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  searchBar: { width: windowWidth, height: 15, backgroundColor: '#fff' },
  searchBarHeader: {
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: 0,
    height: 30,
    backgroundColor: '#fff'
  },
  searchBarInput: {
    marginBottom: 12,
    marginLeft: 12
  },
  searchBarButton: {
    marginBottom: 12,
    marginRight: 12
  }
});
