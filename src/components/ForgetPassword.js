import React from 'react';
import { StyleSheet, Image } from 'react-native';
import {
  Container,
  Form,
  Item,
  Label,
  Input,
  View,
  Button,
  Text
} from 'native-base';
import logo from '../manifest/foodie_logo.png';

class ForgetPassword extends React.Component {
  render() {
    return (
      <Container>
        <View>
          <Image source={logo} style={styles.logo} />
        </View>
        <Form>
          <Item stackedLabel>
            <Label>Email</Label>
            <Input />
          </Item>
        </Form>
        <Button block success style={styles.send}>
          <Text>Send</Text>
        </Button>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    width: '100%',
    height: 200
  },
  send: {
    marginTop: 20,
    marginLeft: 25,
    marginRight: 25
  }
});

export default ForgetPassword;
