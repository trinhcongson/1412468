import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import {
  H2,
  Container,
  Form,
  Item,
  Label,
  Input,
  Button,
  Text
} from 'native-base';
import { axiosInstance as axios } from '../utils';

class Home extends Component {
  static navigationOptions = () => ({
    title: 'Login'
  });

  state = { username: '', password: '' };

  handleChangeUserName = text => {
    this.setState({ username: text });
  };

  handleChangePassword = text => {
    this.setState({ password: text });
  };

  handleGoToRegister = () => {
    this.props.navigation.navigate('Register');
  };

  handleLogin = async () => {
    const { username, password } = this.state;
    const userCredentials = {
      email: username,
      password
    };
    const response = await axios.post(`/login`, userCredentials);
    if (response) {
      this.props.navigation.navigate('App');
    }
  };

  render() {
    const { username, password } = this.state;
    return (
      <Container>
        <Form>
          <Item stackedLabel style={styles.formItem}>
            <Label style={styles.formItemLabel}>Username</Label>
            <Input onChangeText={this.handleChangeUserName} value={username} />
          </Item>
          <Item stackedLabel style={styles.formItem}>
            <Label style={styles.formItemLabel}>Password</Label>
            <Input
              onChangeText={this.handleChangePassword}
              secureTextEntry
              value={password}
            />
          </Item>
        </Form>
        <Button block style={styles.loginButton} onPress={this.handleLogin}>
          <Text>Login</Text>
        </Button>
        <Button
          transparent
          onPress={this.handleGoToRegister}
          style={styles.registerButton}
        >
          <Text style={styles.registerButtonText}>Register</Text>
        </Button>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  loginButton: {
    marginTop: 20,
    marginLeft: 25,
    marginRight: 25
  },
  registerButton: {
    alignSelf: 'center'
  },
  formItem: {
    marginLeft: 25,
    marginRight: 25
  },
  formItemLabel: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  registerButtonText: {
    color: 'red'
  }
});

export default Home;
