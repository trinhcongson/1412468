import React from 'react';
import { Footer, FooterTab, Button, Icon } from 'native-base';

class CustomFooter extends React.Component {
  render() {
    return (
      <Footer>
        <FooterTab>
          <Button>
            <Icon name="home" />
          </Button>
          <Button>
            <Icon name="notifications" />
          </Button>
          <Button>
            <Icon name="cart" />
          </Button>
          <Button>
            <Icon name="person" />
          </Button>
        </FooterTab>
      </Footer>
    );
  }
}

export default CustomFooter;
