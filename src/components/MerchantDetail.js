import React, { Component } from 'react';
import { Image, StyleSheet } from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Text,
  Icon,
  Left,
  Body,
  Right
} from 'native-base';

import { axiosInstance as axios } from '../utils';
import Footer from './Footer';

export default class MerchantDetail extends Component {
  state = {
    merchantDetail: undefined,
    merchantMenu: undefined,
    clickedItem: 0
  };

  fetchMerchantDetailAndMenu = async () => {
    const { id: merchantID } = this.props.navigation.state.params;
    try {
      const response = await axios.get(`/restaurant/getMenu/${merchantID}`);
      const newMenu = response.data.menu.map(food => ({
        ...food,
        quantityOrder: 0
      }));
      if (response) {
        this.setState({
          merchantDetail: response.data.restaurant,
          merchantMenu: newMenu
        });
      }
    } catch (error) {
      console.error(error);
    }
  };

  handleOrderQuantity = (id, type) => {
    if (this.state.clickedItem === id) {
      switch (type) {
        case 'increase': {
          this.setState({ orderQuantity: this.state.orderQuantity + 1 });
          break;
        }
        case 'decrease': {
          this.setState({ orderQuantity: this.state.orderQuantity - 1 });
          break;
        }
      }
    }
  };

  componentDidMount() {
    this.fetchMerchantDetailAndMenu();
  }

  render() {
    return (
      <Container>
        <Content>
          <Card>
            <CardItem>
              <Image
                style={styles.backgroundImage}
                source={require('../manifest/foodie_logo.png')}
              />
              <Left>
                <Text>Test</Text>
              </Left>
            </CardItem>
          </Card>

          {/* {this.state.merchantMenu &&
            this.state.merchantMenu.map(food => (
              <Card key={food.id}>
                <CardItem>
                  <Left>
                    <Image
                      style={styles.foodImage}
                      source={{ uri: food.image }}
                    />
                    <Body>
                      <Text>{food.name}</Text>
                      <Text note>{food.price}</Text>
                    </Body>
                  </Left>
                  <Right style={styles.rightCardItem}>
                    <Icon
                      name="remove-circle"
                      onPress={async () => {
                        await this.setState({ clickedItem: food.id });
                        this.handleOrderQuantity(food.quantityOrder);
                      }}
                      style={styles.minusIcon}
                    />
                    <Text>{food.quantityOrder}</Text>
                    <Icon
                      name="add-circle"
                      onPress={async () => {
                        await this.setState({ clickedItem: food.id });
                        this.handleOrderQuantity(food.quantityOrder);
                      }}
                      style={styles.addIcon}
                    />
                  </Right>
                </CardItem>
              </Card>
            ))} */}
        </Content>
        <Footer />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  foodImage: {
    height: 50,
    width: 50
  },
  addIcon: {
    color: 'green',
    marginLeft: 2,
    marginRight: 2
  },
  minusIcon: {
    color: 'red',
    marginLeft: 2,
    marginRight: 2
  },
  rightCardItem: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover'
  }
});
