import { createStackNavigator, createSwitchNavigator } from 'react-navigation';
import Home from './components/Home';
import Register from './components/Register';
import ForgetPassword from './components/ForgetPassword';
import Merchant from './components/Merchant';
import MerchantDetail from './components/MerchantDetail';
import Basket from './components/Basket';
import DeliveryOrder from './components/DeliveryOrder';

const appStack = createStackNavigator({
  Merchant: {
    screen: Merchant
  },
  DeliveryOrder: { screen: DeliveryOrder },
  Basket: {
    screen: Basket
  },
  MerchantDetail: {
    screen: MerchantDetail
  }
});

const authStack = createStackNavigator({
  Home: {
    screen: Home
  },
  Register: {
    screen: Register
  },
  ForgetPassword: {
    screen: ForgetPassword
  }
});

export default createSwitchNavigator({
  App: appStack,
  Auth: authStack
});
